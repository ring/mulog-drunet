# Robustness to spatially-correlated speckle in plug-and-play PolSAR despeckling
Cristiano Ulondu Mendes, Loïc Denis, Charles Deledalle, Florence Tupin

## Abstract
Synthetic Aperture Radar (SAR) provides valuable information about the Earth’s surface in all-weather and day-and-night conditions. Due to the inherent presence of speckle phenomenon, a filtering step is often required to improve the performance of downstream tasks. In this paper, we focus on dealing with the spatial correlations of speckle, which impacts negatively many of the existing speckle filters. Taking advantage of the flexibility of variational methods based on the plug-and-play strategy, we propose to use a Gaussian denoiser trained to restore SAR scenes corrupted by colored Gaussian noise with correlation structures typical of a range of radar sensors. Our approach improves the robustness of plug-and-play despeckling techniques. Experiments conducted on simulated and real polarimetric SAR images show that the proposed method removes speckle efficiently in the presence of spatial correlations without
introducing artifacts, with a good level of detail preservation. Our method can be readily applied, without network re-training
or fine-tuning, to filter SAR images from various sensors, acquisition modes (SAR, PolSAR, InSAR, PolInSAR), spatial resolution, and even benefit from co-registered muti-temporal stacks, when available.


## Dependencies

Use the package installer [pip](https://pip.pypa.io/en/stable/) to install recursively dependencies from the file 'requirements.txt'.

```bash
pip install -r requirements.txt
```
## Usage

Information about the python functions (tpye of inputs and outputs, etc ...) can be found within their definition (see '.py' files in [Python functions](py_functions/)).

## Quick test

For a quick test, download the notebook [0](example_MuLoG_DRUNet_google_colab.ipynb) only and run it in Google colab.

## Run examples

The notebooks [1](example_MuLoG_DRUNet.ipynb) and [2](example_RABASAR_DRUNet.ipynb) show step-by-step how to filter a SLC PolSAR image with MuLoG and RABASAR respectivly, and display the results.

## Additional results

Additional results of the filtering of SLC simulated PolSAR images and an SLC real PolInSAR image with MuLoG can be found in [Additional_results.pdf](results/Additional_results.pdf).
